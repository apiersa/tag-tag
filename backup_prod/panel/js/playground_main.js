var app = playground({

  width: 128,
  height: 128,

  smoothing: false,

  scaleToFit: true,

  render: function() {
    
  },
  
  mousedown: function(data) {
this.layer.clear("#004");

    this.layer.save();

    this.layer
      .beginPath()
      .circle(this.mouse.x, this.mouse.y, 32)
      .clip();

    this.layer.drawImage(this.images.earth, 0, 0);

    this.layer.restore();
  },

  container: main_canvas  
});