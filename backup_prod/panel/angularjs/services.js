'use strict';

/* Services */

angular.module('tagerApp')
.service('AppService', ['$http', '$q', function ($http, $q) {
		// TODO: harcoded
			//var users_url = 'http://localhost:8000/user';
			var users_url = '/user';

			function getUsers() {
				var request = $http.get(users_url);

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getUser( user_id ) {
				var request = $http.get(users_url+"/"+user_id);
				var user;
				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				$q.reject(reason.data.message);
				});

				return user;
			}


			function getPages( id ) {
				var request = $http.get(users_url + '/' + id + '/page');

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getPage( user_id, page_id ) {
				var request = $http.get(users_url + '/' + user_id + '/page/' + page_id);

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getNotes( user_id, page_id ) {
				var request = $http.get(users_url + '/' + user_id + '/page/' + page_id + '/note');

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getNote( user_id, page_id, note_id ) {
				var request = $http.get(users_url + '/' + user_id + '/page/' + page_id + '/note/'+note_id);

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function saveNote( user_id, page_id, id, note ){
				var request = $http.put(users_url + '/' + user_id + '/page/' + page_id + '/note/' + id, note);

				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

					return $q.reject(reason.data.message);
				});
			}

			function createNote( note ){
				var request = $http.put('saveNote/', note);

				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

					return $q.reject(reason.data.message);
				});
			}

			function deleteNote( id ){
				var request = $http.get('deleteNote/'+id);

				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

					return $q.reject(reason.data.message);
				});
			}

			return ({
				getUsers: function () {
					return getUsers();
				},
				getUser: function ( user_id ) {
					return getUser(user_id);
				},
				getPages: function ( id ) {
					return getPages( id );
				},
				getPage: function ( user_id, page_id ) {
					return getPage( user_id, page_id );
				},
				getNotes: function ( user_id, page_id ) {
					return getNotes( user_id, page_id );
				},
				getNote: function (user_id, page_id, note_id){
						return getNote(user_id, page_id, note_id);
				},
				saveNote: function ( user_id, page_id, id, note ) {
					return saveNote( user_id, page_id, id, note );
				},
				createNote: function ( note ) {
					return createNote( note );
				},

				deleteNote: function ( id ) {
					return deleteNote( id );
				}
			});
	} ]);


	angular.module('tagerApp')
	.service('WwwService', ['$http', '$q', function ($http, $q) {
		function openPage(addr) {
			var request = $http.put('/wwwservice', {"addr": addr});

			return request.then(function (response) {
			return response.data;
			}, function (reason) {
			// jeśli wystąpił nieoczekiwany error
			if (!angular.isObject(reason.data) || !response.data.message)
				$q.reject("An unknown error occurred.");

			// w innym przypadku użyj oczekiwanej wiadomości błędu
			return $q.reject(reason.data.message);
			});

		}

			return ({
				openPage: function ( addr ) {
					return openPage(addr);
				}
			})
	}]);
