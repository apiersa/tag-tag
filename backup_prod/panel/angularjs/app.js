'use strict';

/* App Module */

var app = angular.module('tagerApp', ['ngCookies'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
