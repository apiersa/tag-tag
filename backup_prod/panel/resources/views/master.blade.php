<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="tagerApp">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=yes">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- style.css -> Layout -->
    <link rel="stylesheet" type="text/css"
					href="<?= asset('css/style.css') ?>">

    <!-- draw.css -> Modul rysowania -->
    <link rel="stylesheet" type="text/css"
					href="<?= asset('css/draw.css') ?>">

    <!-- Ikony font-awesome -->
    <link rel="stylesheet" type="text/css"
					href="<?= asset('css/font-awesome.min.css') ?>">

    <!-- Biblioteki zewnetrzne: jQuery, Angular.js -->
    <script src="<?= asset('js/lib/jquery.js') ?>"></script>
    <script src="<?= asset('angularjs/lib/angular.min.js') ?>"></script>
    <script src="<?= asset('angularjs/lib/angular-cookies.min.js') ?> "></script>

    <title>Tago.pro</title>
    <script type="text/javascript">
        if((navigator.userAgent.match(/iPad/i))) {
            document.write("<link type=\"text\/css\" rel=\"stylesheet\" media=\"all\" href=\"css/ipad.css\" charset=\"utf-8\" \/>");
        }
  	    else if((navigator.userAgent.match(/iPhone/i))||(navigator.userAgent.match(/iPod/i))) {
  	            document.write("<link type=\"text\/css\" rel=\"stylesheet\" media=\"all\" href=\"css/iphone.css\" charset=\"utf-8\" \/>");
  	    }
  	    else if((navigator.userAgent.match(/Android/i))||(navigator.userAgent.match(/iPod/i))) {
  	            document.write("<link type=\"text\/css\" rel=\"stylesheet\" media=\"all\" href=\"css/android.css\" charset=\"utf-8\" \/>");
        }
</script>
</head>

<!--------------------------- BODY SECTION ------------------------------------>

<body ng-controller="mainController">
    <div class="container">
        <div class="www_frame" id="mobile-www-frame">
            <!-- element div, na ktorym odbywa sie rysowanie figur -->
            <div id="main_canvas"></div>
<!--Funkcja redraw (drawing.js) odpowiada za przesuwanie figur przy scroll... ->
<!-- TODO: do jQuery -->
            <div id="web_div">
              <!-- onscroll="redraw('web_div')" -->
<!-- element iframe, w ktorym wyswietlane sa tagowane strony www -->
                <iframe id="web_view" 
												frameborder=0
												scrolling="yes"></iframe>
            </div>
        </div>
    </div>
    <div class="header-box" id="header-box">
        <div class="mobile-left-navi">
            <center><a id="mobile-task-see-btn" class="mobile-task-see-btn" ><i class="fa fa-chevron-right"></i></a></center>
            <center><a id="mobile-task-hide-btn"  class="mobile-task-hide-btn"><i class="fa fa-chevron-left"></i></a></center>
        </div>
        <script>
            $("#mobile-task-see-btn").click(function () {
                document.getElementById("right-box-content").style.display = "none";
                $("#right-box").addClass("mobile-right-box");
                $("#header-box").addClass("mobile-active-box");
                document.getElementById("mobile-www-frame").style.paddingRight = "0";
                document.getElementById("mobile-task-hide-btn").style.display = "inline-block";
                document.getElementById("mobile-task-see-btn").style.display = "none";
            });
            $("#mobile-task-hide-btn").click(function () {
                document.getElementById("right-box-content").style.display = "block";
                $("#right-box").removeClass("mobile-right-box");
                $("#header-box").removeClass("mobile-active-box");
                document.getElementById("mobile-www-frame").style.paddingRight = "250px";
                document.getElementById("mobile-task-see-btn").style.display = "inline-block";
                document.getElementById("mobile-task-hide-btn").style.display = "none";
                document.getElementById("mobile-input-box-1").style.display = "none";
            });
            $(function () {
            $("#see-mobile-input-box-1").click(function () {
                $("#mobile-input-box-1").slideToggle();
            });
            $("#my-refresh-ico").click(function () {
                var iframe = document.getElementById('web_view');
                iframe.src = iframe.src;
            });
        });
        </script>
        <table class="header-width-table">
            <tbody>
                <tr>
                    <td class="td-logo">
                        <div class="logo-box"><img src="http://keysoft.pro/img/logo.png"></div>
                    </td>
                    <td class="td-logo-btns">
                        <div class="navi-head-btns">
                            <a href="#" class="back-head-btn"><i class="fa fa-angle-left"></i></a>
                            <a href="#" class="forward-head-btn"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </td>
                    <td class="td-logo-input">
                        <div class="head-input">
                            <input type="text" value="<% selected_page.name %>" id="main-domain-input" onkeypress="handleKeyPress(event)"><a class="refresh-ico" id="my-refresh-ico"><i class="fa fa-refresh"></i></a>
                            <script>
                            function handleKeyPress(e){
                                 var key=e.keyCode || e.which;
                                  if (key==13){
                                     var adr = $('#main-domain-input').val();
                                     $('#web_view').attr('src', 'https://' + adr);
                                  }
                                }
                            </script>
                        </div>
                    </td>
                    <td class="td-logo-input-btn">
                        <div class="navi-right-btn">
                            <a class="right-navi-input" id="see-mobile-input-box-1"><i class="fa fa-pencil-square-o"></i></a>
                        </div>
                    </td>
                    <td class="td-logo-warning">
                        <div class="navi-right-btn">
                            <a href="#" class="right-navi-atten"><i class="fa fa-exclamation-triangle"></i></a>
                        </div>
                    </td>
                    <td class="td-logo-warning">
                        <div class="navi-right-btn">
                            <a href="#" class="right-navi-atten" id="mytest-a"><i class="fa fa-exclamation-triangle"></i></a>
                        </div>
                    </td>
                    <td class="td-logo-warning standard-top-btn right">
  	                        <div class="navi-right-btn">
  	                            <a class="right-navi-reset" ng-click="ResetMyResolution()"><i class="fa fa-expand"></i></a>
  	                        </div>
  	                    </td>
  	                    <td class="td-logo-warning mobile-top-btn right">
  	                        <div class="navi-right-btn">
  	                            <a class="right-navi-reset" ng-click="ResetMyResolutionMobile()"><i class="fa fa-expand"></i></a>
  	                        </div>
  	                    </td>
                    <td class="td-logo-options">
                        <a href="/test" class="various fancybox.ajax right-setting-ico" title="testowy"><i class="fa fa-gear"></i></a>
                    </td>
                    <td class="right-menu-td" style="width:250px;"></td>
                </tr>
            </tbody>
        </table>
        <table class="header-width-table header-width-table-1" id="mobile-input-box-1" style="display:none;">
            <tbody>
                <tr>
                    <td class="td-logo-btns mobile-td-butons">
                        <div class="navi-head-btns">
                            <a href="#" class="back-head-btn"><i class="fa fa-angle-left"></i></a>
                            <a href="#" class="forward-head-btn"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </td>
                    <td class="td-logo-input">
                        <div class="head-input">
                            <input type="text" value="domena.pl"><a href="#" class="refresh-ico"><i class="fa fa-refresh"></i></a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <!--
        <div class="header-search">
        </div>
        -->
    </div>

<!--------------------------- Menu uzytkownika -------------------------------->
    <div class="right-box" id="right-box" ng-init="runApp()">

<!--------------------------- Aktualny user ----------------------------------->
        <div id="right-box-content">
        <div class="employer">
            <table class="employer-table">
                <tr ng-click="getUsers()">
										<!-- TODO: avatars -->
										<td class="img">
                        <img src="img/avatar-2.jpg" class="mega-image">
                    </td>

                    <td>
                        <p class="name">
                            <% selected_user.imie %>
                                <% selected_user.nazwisko %>
                        </p>
                        <p class="title">
                            <% selected_user.description %>
                        </p>
                    </td>

                    <td style="width:32px;">
                        <a href="#" class="drop-button employer-1" id="emplo">
                            <i class="fa fa-caret-down"></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>

<!-------------------------- Aktualnie wybrana strona ------------------------->
        <div class="domain">
            <table class="domain-table" cellspacing="0" cellpadding="0">
                <tr ng-hide="selected_page == null" id="pages-tr" ng-click="getPages( selected_user.id, false )">
                    <td class="top next-top">
                        <p class="domain-link">
                            <a href="" id="page-id-domain">
                                <% selected_page.name %>
                            </a>
                            <span ng-show="selected_page.note_important_count > 0"
																	class="number-warning">
																<% selected_page.note_important_count %>
														</span>
                        </p>
                        <p class="domain-link-bottom">
                            <span ng-show="selected_page.note_notended_count > 0">
                              <span class="number acti">
															  <% selected_page.note_notended_count %>
														  </span> aktywne
                            </span>

                          <span>
                            <span class="number lf-m">
															<% selected_page.note_count %>
														</span> wszystkie
                          </span>
                        </p>
                    </td>

                    <td style="width:34px;" class="top">
                        <a href="#" class="drop-button employer-2" id="click">
                            <i class="fa fa-caret-down"></i>
                        </a>
                    </td>
                </tr>
<!------------------------- Wyszukiwarka -------------------------------------->
                <tr>
                    <td colspan="2" class="domain-input-td">
                        <input type="text"
															 ng-model="searchPhrase"
															 class="domain-input"
															 placeholder="Przeszukaj...">
                        <!--<a href="#" class="domain-ico">
                            <i class="fa fa-search"></i>
                        </a>-->
                    </td>
                </tr>
            </table>
<!----------------------------- Lista userow ---------------------------------->
            <div id="see-emplo" style="display:none;">
                <table class="employer-table-2" cellspacing="0" cellpadding="0">
                    <tr class="tr-hover-2"
												id="tr-hover-employer"
												ng-repeat="user in users | filter: { full_name: searchPhrase }">

                        <td class="img">
                            <img src="img/avatar-2.jpg" class="mega-image">
                        </td>

                        <td ng-click="selectUser($index)">
                            <p class="name">
                                <% user.imie %>
                                    <% user.nazwisko %>
                            </p>
                            <p class="title">
                                <% user.description %>
                            </p>
                        </td>
                    </tr>
                </table>
            </div>

<!----------------------------- Lista stron usera ----------------------------->
            <div id="see" style="display:none;">
                <table class="domain-table" cellspacing="0" cellpadding="0">
                    <tr class="tr-hover"
												id="tr-hover"
												ng-repeat="page in pages | filter: { name: searchPhrase }">

                        <td class="top next-top domain-list-td" ng-click="selectPage($index); changeMainDomain($index); ResetMyResolution()">
	                                <input hidden="hidden" id="main-domain-list-<% $index %>" value="<% page.name %>" />
                            <p class="domain-link">
                                <a href="#">
                                    <% page.name %>
                                </a>
                                <span ng-show="page.note_important_count > 0"
                                      class="number-warning">

											<% page.note_important_count %>

								</span>
                            </p>
                            <p class="domain-link-bottom">
                              <span class="number acti">
                                  <% page.note_notended_count %>
                              </span> aktywne
                              <span class="number lf-m"><% page.note_count %></span>
                              wszystkie
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

<!------------------------------ Lista taskow ---------------------------------->

<!-- dzwiek odtwarzany po kliknieciu zielonego przycisku (zatwierdzenie taska) -->
        <audio id="audio" src="<?= asset('sounds/accept.wav') ?>"></audio>
        <div class="task" id="no-see" style="overflow: auto; height: 100%" onscroll="">
            <table class="task-table" cellspacing="0" cellpadding="0">
                <tr class="tr-hover-3"
                    ng-show="keep_empty_note == false"
										ng-click="selectNote(note.index); changeInputDomain(note.id); changeMainResolution(note.id)"
										id="tr-hover-3"
										ng-class="{'note-selected': note.id == selected_note.id}"
										ng-repeat="note in notes | limitTo:quantity | filter: { name: searchPhrase }">

                    <td ng-class="{'top': !note.ended}" class="top">
                        <p class="task-link">
                          <a id="page-id-<% note.id %>"><% note.name %></a>
                            <input hidden="hidden" id="main-domain" value="<% selected_page.name %>" />
                        </p>

                        <p class="task-link-bottom" ng-hide="note.id == selected_note.id">
                            <% note.content | limitTo:15 %>
                        </p>

<!----------------------- ONLY SELECTED NOTE ---------------------------------->
                        <p class="task-link-bottom"
                           style="max-width: 150px; word-wrap: break-word;"
                           ng-show="note.id == selected_note.id">
                            <% note.content %>
                        </p>
                        <p class="task-link-bottom resolution-box"
	                           style="max-width: 150px; word-wrap: break-word;"
	                           ng-show="note.id == selected_note.id">
	                            Szerokość okna <% note.resolution %>
	                                <input hidden="hidden" id="main-resolution-<% note.id %>" value="<% note.resolution %>" />
	                      </p>
                        <p ng-show="show_draw_tools && note.id == selected_note.id">
                            <span class="add-text-table-div edit-task-tool-box">
                            <button class="rectangle-btn"
                                    ng-class="{'selected-tool-btn' : draw_tool == 'rectangle'}"
                                    ng-click="setTool('rectangle')">

                                <i class="fa fa-square-o"></i>
                            </button>

                            <button class="circle-btn"
                                    ng-class="{'selected-tool-btn' : draw_tool == 'circle'}"
                                    ng-click="setTool('circle')">
                                <i class="fa fa-circle-thin"></i>
                            </button>

                            <button class="last-btn" ng-click="cancelDraw()" style=" /* TODO: refactoring */
																				border: none;
																				cursor: pointer;">
                                <i class="fa fa-times"></i>
                            </button>
                        </span>
                        </p>
<!------------------------- END ONLY SELECTED NOTE ---------------------------->
                    </td>
                    <td data-id="<% note.index %>" ng-class="{'top': note.ended == 0, 'hidden': note.ended == 1}" class="right test-right">
                        <p class="task-link-bottom task-bottom-link">
                            <button class="left-edit-button" ng-click="deleteTask( note.id, note.index )"
                                    style=" /* TODO: refactoring */
														 border: none;
														 cursor: pointer;" ng-show="note.id == selected_note.id">

                                <i class="fa fa-times"></i>
                            </button>
                            <button class="right-edit-button" ng-click="addTag(); $event.stopPropagation();"
                                    style=" /* TODO: refactoring */
							    					 border: none;
														 cursor:pointer;" ng-show="note.id == selected_note.id">

                                <i class="fa fa-pencil"></i>
                            </button>
                        </p>
                        <i ng-show="note.important == 1 && note.ended == 0"
                           ng-click="makeImportantTask( note, note.index )"
                           class="fa fa-exclamation-circle warning"></i>
                        <i ng-show="note.important == 0 && note.ended == 0"
                           class="fa fa-exclamation-circle no-warning"
                           ng-click="makeImportantTask( note, note.index )">
												</i>
                        <a href="#">
                            <i class="fa fa-check-circle hidden accept"
                               ng-click="endTask(note, note.index)"
                               style="display:none;"></i>
                            <i ng-show="note.ended == 0" class="fa fa-circle accept look"></i>
                        </a>
                        <i ng-click="restartTask( note, note.index )"
                           ng-show="note.ended == 1"
                           class="fa fa-arrow-circle-o-up hidden"></i>
                        <i ng-show="note.ended == 1" class="fa fa-check-circle hidden accept"></i>
                    </td>
                        <td class="mobile-clear-td"></td>
                </tr>
                <tr ng-show="isNoteLoading">
                    <td colspan="2" style="border:0;">
                        <center>
                            <p class="load-ico">
                              <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw margin-bottom"
                                 style="margin-bottom:160px"></i>
                            </p>
                        </center>
                    </td>
                </tr>

            </table>
        </div>
        </div>
<!------------------------ DOLNE MENU - Dodawanie taskow ---------------------->
        <div class="clear"></div>
            <div class="add-sound" id="sound" style="display:none;">
                <table class="add-sound-table">
                    <tr>
                        <td colspan="2">
                            <p class="text-sound-page">/kontakt</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="bbc-table">
                                <a href="#" class="bbc first"><i class="fa fa-tint"></i></a>
                                <a href="#" class="bbc"><i class="fa fa-remove"></i></a>
                                <a href="#" class="bbc"><i class="fa fa-random"></i></a>
                                <a href="#" class="bbc"><i class="fa fa-clone"></i></a>
                                <a href="#" class="bbc"><i class="fa fa-bold"></i></a>
                                <a href="#" class="bbc"><i class="fa fa-link"></i></a>
                                <a href="#" class="bbc last"><i class="fa fa-chevron-right"></i></a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50px;">
                            <a href="#" class="rec"><i class="fa fa-circle"></i></a>
                        </td>
                        <td>
                            <p class="rec-info"> Nagrywanie </p>
                            <p class="rec-time"> 00:00:00</p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="add-text" id="text" style="display:none;">
                <table class="add-text-table">
                    <tr id="task-hidden-2">
                        <td>
                            <p class="text-sound-page">
                                <% selected_note_name %>
                            </p>
                        </td>
                    </tr>
                    <tr id="task-hidden-1">
                        <td style="text-align: right;">
                            <div class="add-text-table-div">
                                <button class="rectangle-btn" ng-class="{'selected-tool-btn' : draw_tool == 'rectangle'}" ng-click="setTool('rectangle')">
                                    <i class="fa fa-square-o"></i>
                                </button>
                                <button class="circle-btn" ng-class="{'selected-tool-btn' : draw_tool == 'circle'}" ng-click="setTool('circle')">
                                    <i class="fa fa-circle-thin"></i>
                                </button>
                                <div class="clear"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea rows="3" id="task-description" class="text-task" placeholder="opisz zadanie"></textarea>
                            <input id="resolution" hidden="hidden" value="" />
                        </td>
                    </tr>
                    <!--<tr>
                        <td>
                            <a id="save-task-link" href="#" ng-click="saveTask(true)" onclick="taskLoadingStart()" class="bbc">
                                <i class="fa fa-save"></i>
                            </a>

                        </td>
                    </tr>-->
                </table>
            </div>
            <div class="bottom-buttons">
                <table class="buttons" cellspacing="0" cellpadding="0">
                    <tr>
                        <!--<td><a href="#" class="left" id="sound-on"><i class="fa fa-microphone"></i></a>
                        </td>-->
                        <td id="add-text-btn-td">
                            <a id="text-on"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td id="exit-text-btn-td" style="display:none;">
                            <a id="text-on-1" ng-click="saveTask(true)" onclick="taskLoadingStart()" class="add-text-save-btn"><i class="fa fa-save"></i></a>
                            <a id="text-on-2" class="add-text-exit-btn"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <script src="<?= asset('js/events.js') ?>"></script>
    <!-- ANGULAR.JS -->
    <script src="<?= asset('angularjs/app.js') ?>"></script>
    <script src="<?= asset('angularjs/services.js') ?>"></script>
    <script src="<?= asset('angularjs/controllers.js') ?>"></script>
    <!---------------->
    <script src="<?= asset('js/drawing.js') ?>"></script>

    <script>
	            function resizeWin() {
			          window.self.resizeTo(1024,screen.availHeight);
	            }
	  </script>

</body>
</html>
