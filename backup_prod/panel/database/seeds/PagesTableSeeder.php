<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
		foreach(range(1, 10) as $v){
		   DB::table('pages')->insert([
				'name' => $faker->domainName,
				'user_id' => rand(1, 5)
			]);
	   }
    }
}
