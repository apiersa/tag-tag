<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index', ['middleware' => 'cors']);
Route::get('/test', 'MainController@ajax');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::put('wwwservice', 'WwwController@index');


Route::put('saveNote', 'NoteController@saveNote');

Route::get('deleteNote/{id}', function ($id) {
    return App\Note::destroy($id);
});

Route::get('addPage/{name}', function ($name) {
    $page = new App\Page;
    $page->name = $name;
    $page->user_id = 1;

    if($page->save())
		{
			return json_decode(true);
		}
		else
		{
			return json_decode(false);
		}
});

Route::group(['middleware' => ['web']], function () {
    //
});

Route::resource('user', 'UserController',
                ['only' => ['index', 'show']]);

Route::resource('user.page', 'PageController',
                ['only' => ['index', 'show']]);

Route::resource('user.page.note', 'NoteController',
                ['only' => ['index', 'show', 'update']]);
