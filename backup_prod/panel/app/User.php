<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

     /**
     * Get all of the posts for the user.
     */
    public function pages()
    {
        return $this->hasMany('App\Page');
    }

}
