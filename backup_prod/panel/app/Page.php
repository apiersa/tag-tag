<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Page extends Model
{
    protected $appends = array('note_notended_count', 'note_important_count', 'note_count');

	public function notes()
    {
        return $this->hasMany('App\Note')->orderby('important', 'desc')->orderby('ended', 'asc');
    }

	public function getNoteNotendedCountAttribute(){
		return Note::whereRaw('ended IS FALSE AND page_id = ?', array($this->id))->count();
	}

	public function getNoteImportantCountAttribute(){
		return Note::whereRaw('important IS TRUE AND page_id = ?', array($this->id))->count();
	}

	public function getNoteCountAttribute(){
		return Note::whereRaw('page_id = ?', array($this->id))->count();
	}

	/*public function getEndedCountAttribute()
    {
        return $this->notes->getEnded();
    }*/

}
