<?php

use Illuminate\Database\Seeder;

class NotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker = Faker\Factory::create();
		foreach(range(1, 20) as $v){
		   DB::table('notes')->insert([
				'content' => $faker->paragraph,
				'shape' => 'RECT',
				'page_id' => rand(1, 10),
				'important' => $faker->boolean,
				'ended' => $faker->boolean
			]);
	   }
    }
}
